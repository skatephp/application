<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 27.08.2017
 * Time: 10:09
 */

namespace src\Controllers;

use Skate\Controller;

class BaseController extends Controller
{
    public function __construct()
    {
        $this->template->setBaseDir(TEMPLATES_PATH);
    }
}
