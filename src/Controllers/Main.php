<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 27.08.2017
 * Time: 10:04
 */

namespace src\Controllers;

class Main extends BaseController
{
    public function page()
    {
        $this->template->render('main');
    }
}
