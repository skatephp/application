<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 20.08.2017
 * Time: 15:12
 */

use Skate\Core\Route;

/**
 * @var \Skate\Core\Router $router
 */
$router->add(new Route('/', 'Main', 'page'));
